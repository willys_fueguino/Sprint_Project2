## Curso de Desarrollo Web Backend de Acámica

#### Descripción

Sprint Project Nº2 del curso de Back End. (API Persistente)

#### Instrucciones de instalación:

1. Instalar git, node.js, npm y MongoDB Compass en su computadora.
2. Crear carpeta para su instalación
3. Ingresar a la carpeta creada y desde la consola ejecutar:
   `git clone https://github.com/willysfueguino/Sprint_Project2.git`
3. Ingresar a la carpeta a través de la consola Sprint_Project2.
4. Ejecutar el comando `npm install`.
5. Copiar el archivo .env.example a .env
6.  Crear base de datos en MongoDB Compass con la coleccion "usuarios"
7. Configurar las variables de entorno del archivo .env a su configuración
8. Chequear las dependencias utilizadas vía `npm-check`
9. Arrancar el servidor ejecutando el comando`nodemon ./api/app.js`

#### Documentación de la aplicación con SWAGGER:

- Ingresar a la URL http://localhost:5000/api-docs
- Loguearse con alguno de los usuarios de muestra (se pueden ver los datos en la base de datos mongodb).
- Copiar el token de usuario.
- Hacer click en el boton verde que dice "Authorize" en la interfaz de Swagger (esquina superior derecha).
- Pegar solo el toquen en el campo de texto "value" y darle a "Authorize".
- Testear end points.

---