const productos = require("../models/productos");
const asyncRedis = require("async-redis");    
const client = asyncRedis.createClient(); 

exports.findProducts = async function(req, res, next){
    listaProductos = await productos.find({$and: [{ "__v": {$eq: '0'}}]},{ codigo:1, nombre:1, descripcion:1, tamanio:1, precio:1,  '_id': false})
    
    await client.set('listaProductos', JSON.stringify(listaProductos), 'EX', '60');

    cachedProducts = await client.get('listaProductos')
    cachedProducts = JSON.parse(cachedProducts)
    
    res.status(201).json(cachedProducts)
    next()
}

exports.createProduct = async function (req,res, next) {
  let {codigo, nombre, descripcion, tipo, tamanio, precio, stock} = req.body;

  if (codigo == undefined || nombre == undefined || tipo == undefined ){
    return res.status(500).send("Datos de producto incompletos o faltantes. ")
  }
  
  let product = new productos({
    codigo: codigo,
    nombre: nombre,
    descripcion: descripcion,
    tipo: tipo,
    tamanio: tamanio,
    precio: precio,
  });
  
  try{
    await product.save()
  
    res.status(201).json("el producto " + product.nombre + " fue creado exitosamente")
  }
  catch (err){
    res.status(500).send("Error")
  }
}

exports.updateProducts =async function(req, res, next){
  let {codigo, nvocodigo, nombre, descripcion, tipo, tamanio, precio, stock} = req.body;
  
  try{
  let codigoEncontrado = await productos.findOne( {codigo} )
  if (!codigoEncontrado ){
    return res.status(400).json("Codigo de producto a modificar incorrecto.")
  }
  }
  catch (err){
    next()
  }

  try{
    let productoActualizado = await productos.findOneAndUpdate(
        { codigo: codigo },
        { $set: {codigo: nvocodigo,
          nombre: nombre,
          descripcion: descripcion,
          tipo: tipo,
          tamanio: tamanio,
          precio: precio
          }},
          { upsert: true,
            returnNewDocument: true }
        )
  
      res.status(201).json("El producto  fue actualizado exitosamente.")
    }
    catch(err){
      console.log(err)
      res.status(500).send("Error interno.")
    }
}