const {generateJwt} = require("../helpers/generateJwt");
const jwt = require("jsonwebtoken")
const usuarios = require("../models/usuarios")

exports.buscaMail = async function(req, res, next) {
  try{
      //console.log("estoy en buscaMail")
      let email = req.body.email


      let mailEncontrado = await usuarios.findOne( {email} )//.select({'email':1, '_id': false});
      //console.log({email: email})
      //console.log("|"+mailEncontrado+"|")
      if (!mailEncontrado ){
        next()
      }
      
      else {
        res.status(400).json("El mail ingresado ya está registrado")
      }
      
    }
    catch (err) {
      console.log(err)
    }    
}

// Login
exports.signin = async function signin(req, res, next) {

  let email =  req.body.email
  let emailHeaders = req.headers.email
  let password = req.body.password
  let token = undefined

try {           ///////////// VERIFICACION EMAIL PASADO POR BODY
    
    let verificarMail = await usuarios.findOne( {email: email} ,{email:1, '_id': false})
    let verificarPass = await usuarios.findOne({ pass: {$eq: password}} ,{ pass:1,  '_id': false})
    // if(!verificarMail){
    //   return res.status(403).json("Usuario no registrado o email incorrecto.")
    // }

    //console.log("verificarMail = " + verificarMail)
    //console.log("verificarPass = " + verificarPass)
    if(verificarMail != "" && verificarPass != "" ){
      token = await generateJwt(verificarMail, process.env.JWT_SECRET_KEY);
      res.status(200).json({ status: "signin", token });      
      userToken = await usuarios.findOneAndUpdate(
        {email},
        { token: token },
        { new: true }
      
      )
    } else {
      
      return res.status(403).send("Datos de usuario incorrectos.")
    }
    
  } catch (err) {
      console.log("error mail body = " +err);
  }
  
  try {                  ///////////// VERIFICACION EMAIL PASADO POR HEADERS
    
    let verificarMail = await usuarios.findOne( {email: emailHeaders} ,{email:1, '_id': false})
    let verificarPass = await usuarios.findOne({ pass: {$eq: password}} ,{ pass:1,  '_id': false})
    if(!verificarMail){
      return res.status(403).json("Usuario no registrado o email incorrecto.")
    }

    //console.log("verificarMail = " + verificarMail)
    //console.log("verificarPass = " + verificarPass)
    if(verificarMail != "" && verificarPass != "" ){
      token = await generateJwt(verificarMail, process.env.JWT_SECRET_KEY);
      res.status(200).json({ status: "signin", token });      
      userToken = await usuarios.findOneAndUpdate(
        {emailHeaders},
        { token: token },
        { new: true }
      
      )
    } else {
      
      return res.status(403).send("Datos de usuario incorrectos.")
    }
    
  } catch (err) {
    console.log("error mail body = " +err);
  }
    //console.log("user Token: " + token)
    next()
    
  };

// Autenticar
exports.authenticated = async function authenticated(req, res, next) {
  let email = req.body.email
  let emailHeaders = req.headers.email
  let emailCorrecto = ""
  //console.log(req.headers.authorization)
  if (email === undefined) {
    emailCorrecto = emailHeaders
  } else if(emailHeaders === undefined) {
    emailCorrecto = email
  } else {
    res.status(403).json("Error de credencial de usuarios")
  }

  //console.log("email = |" + email + " emailHeaders = |" +  emailHeaders)

  try {
      let token = req.headers.authorization
      //console.log({token: token})
      if(!token){ return res.status(403).json("Error de credenciales de usuario")}
      if(token === undefined ){ return res.status(403).json("Error de credenciales de usuario")}

      token = req.headers.authorization.split(" ")[1];
      //res.send(JSON.stringify(token))
      jwt.verify(token, process.env.JWT_SECRET_KEY, (err, authData) => {
        if (err) {
          if(err.message == "jwt malformed"){
            res.status(403).send("Usuario no logueado.")
          }
          if ( err.message == 'jwt expired' ){
            res.status(403).send("Sesión de usuario expirada.")
          }
          
        } else {
          
          let base64Url = token.split('.')[1];
          let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
          let jsonPayload = decodeURIComponent(Buffer.from(base64, 'base64').toString().split('').map(function(c) {
              return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          }).join(''));
          //console.log(jsonPayload)
          jsonPayload = jsonPayload.split(':')[2]
          jsonPayload = jsonPayload.split('}')[0]
          //console.log(jsonPayload)

          //console.log(`"${emailCorrecto}"` + jsonPayload)
          if( `"${emailCorrecto}"` != jsonPayload){
            return res.status(403).json("El mail ingresado no coincide con su credencial de autorizacion.")
          } 
          else {
            next();
          }
        }


      });
    
  } catch (err) {
      console.log(err)
  }
};

// Usuario es admin
exports.isAdmin = async function isAdmin(req, res, next) {
  let email = req.body.email
  let emailHeaders = req.headers.email
  let emailCorrecto = ""
  //console.log(req.headers.authorization)
  if (email === undefined) {
    emailCorrecto = emailHeaders
  } else if(emailHeaders === undefined) {
    emailCorrecto = email
  } else {
    res.status(403).json("Error de credencial de usuarios")
  }

  if(!emailCorrecto) {

     res.status(403).json("Email incorrecto.")
    }

  try{
    
      let verificarMail = await usuarios.findOne({ $and: [{ email: {$eq: emailCorrecto}}, {isAdmin: {$eq: 'true'}}]},{ email:1, isAdmin:1,  '_id': false})
      //console.log("verificar email " + verificarMail)
      if (verificarMail != null) {
        next()
      }
      //   console.error("Acceso denegado: ");
      //   return res.status(403).send({ status: "Acceso denegado" });
      // } else {
      //   //console.log("AUTORIZADO")
      //   next();
      // }
    }
  catch(err){
    console.log(err.message)
    return res.status(500).send("Error interno.")
  }
};
